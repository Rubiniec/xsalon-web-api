

const pg = require('pg');
var types = require('pg').types;
types.setTypeParser(1700, function(val) {
  return parseFloat(val);
});

const { Pool } = pg;

let localPoolConfig = {
  user: 'postgres',
  password: 'z',
  host: 'localhost',
  port: '5432',
  database: 'postgres'
};
function GetTableName(table) {
  return " " + "gascom" + "." + table;
}

const poolConfig = process.env.DATABASE_URL ? {
  connectionString: process.env.DATABASE_URL,
  ssl: {
    rejectUnauthorized: false
  }
} : localPoolConfig;

const pool = new Pool(poolConfig);
module.exports = {
  pool: pool,
  GetTableName
}
