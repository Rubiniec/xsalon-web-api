

const pool = require('../utils/db').pool;





class BaseDB {
    constructor(tablename) {
        this.tableName = tablename;
        this.pool = pool;
        this.sa = 1;

    }

    mapujNaObiekty(wynikZapytania, klasa) {
        return wynikZapytania.map(wiersz => this.mapujNaObiekt(wiersz, klasa));
    }

    mapujNaObiekt(wiersz, klasa) {
        const obiekt = new klasa();
        for (const klucz in wiersz) {
            if (obiekt.hasOwnProperty(klucz)) {
                obiekt[klucz] = wiersz[klucz];
            }
        }
        return obiekt;
    }

    async wykonajZapytanie( query, result ) {
        try {
            const _res = await query;
            if( result != undefined && result != null)
            Object.assign(result, _res.rows);
            return true; // Jeśli zapytanie wykonało się bez problemu, zwróć true
        } catch (error) {
            console.error('Błąd podczas wykonywania zapytania:', error);
            return false; // Jeśli wystąpił błąd, zwróć false
        }
    }

    async wykonajZapytanie1(query, result ) {
        try {
            const _res = await query;
            if( result != undefined && result != null)
            Object.assign(result, _res.rows[0]);
            return  {  status :{  stat  : 0 ,  desc : ''} }; // Jeśli zapytanie wykonało się bez problemu, zwróć true
        } catch (error) {
            return  {  status :{  stat  : 1 ,  desc : 'error'} };
        }
    }



    async  TrnStart() {
        try {
            await pool.query('BEGIN');
            console.log('Rozpoczęto transakcję');
        } catch (error) {
            console.error('Błąd podczas rozpoczynania transakcji:', error);
            throw error; // Rzucenie błędu w przypadku problemów z rozpoczęciem transakcji
        }
    }

    async  TrnCommit() {
        try {
            await pool.query('COMMIT');
            console.log('Zatwierdzono transakcję');
        } catch (error) {
            console.error('Błąd podczas zatwierdzania transakcji:', error);
            throw error; // Rzucenie błędu w przypadku problemów z zatwierdzaniem transakcji
        }
    }

    async  TrnRollback() {
        try {
            await pool.query('ROLLBACK');
            console.log('Cofnięto transakcję');
        } catch (error) {
            console.error('Błąd podczas cofania transakcji:', error);
            throw error; // Rzucenie błędu w przypadku problemów z cofaniem transakcji
        }
    }


    getColumns(obj) {
        let i = 1;
        const columns = Object.entries(obj).reduce((acc, [key, value]) => {
            if (value !== '__Key') {
                acc.columns.push(key);
            } else {
                acc.keys.push(`${key} = $${i}`);
                i++;
            }
            return acc;
        }, {columns: [], keys: []});

        const columnsStr = columns.columns.join(', ');
        const keysStr = columns.keys.join(` AND `);
        return {columnsStr, keysStr};
    }

    async select(id, column) {
        let sql;
        let values;
        if (id === undefined) {
            sql = `SELECT * FROM ${this.tableName};`; // Zapytanie bez WHERE, gdyż chcemy wszystkie rekordy
            values = [];
        } else {
            sql = `SELECT * FROM ${this.tableName} WHERE ${column} = $1`; // Zapytanie z WHERE id = $1, gdyż chcemy rekord o konkretnym id
            values = [id];
        }

        console.log(sql, values); // do debugowania
        let result = {};
        try {
            result = await pool.query(sql, values);
            return result.rows;
        } catch (error) {
            console.log(error);
        }
        return result;
    }

    /**
     * Asynchroniczna funkcja do wykonywania zapytania SELECT na podstawie podanych warunków i kolumn.
     * @param {object} conditions Obiekt zawierający warunki zapytania.
     * @param {string} _columns Opcjonalny parametr - kolumny do wybrania, domyślnie wszystkie (*).
     * @param {string} _tabela Opcjonalny parametr - nazwa tabeli, domyślnie wartość tableName z obiektu wywołującego.
     * @returns {Promise<Array<Object>>} Promise zawierający wyniki zapytania w postaci tablicy obiektów.
     */
    async selectParam( conditions, _columns, _tabela, _joinTable) {
        let whereClause = '';
        let values = [];

        if (conditions && typeof conditions === 'object' && Object.keys(conditions).length > 0) {
            const whereConditions = Object.keys(conditions).map((key, index) => {
                values.push(conditions[key]);
                return `${key} = $${index + 1}`;
            });
            whereClause = ` WHERE ${whereConditions.join(' AND ')}`;
        }
        let cols = ' * ';
        // Sprawdzenie, czy został przekazany opcjonalny parametr
        let tabela ='';
        if (_columns !== undefined) {
            cols = _columns;
        }

        if (_tabela !== undefined) {
            tabela = _tabela;
        }
        else
        {
          tabela = this.tableName;
        }
        const sql = `SELECT  ${cols}  FROM ${tabela}${whereClause};`;
        let result = {};
        console.log(sql, values); // do debugowania
        try {
            result = await pool.query(sql, values);
            return result.rows;
        } catch (error) {
            console.log(error);
        }
        return result;
    }

    /**
     * Asynchroniczna funkcja do wykonywania zapytania SELECT na podstawie podanych warunków i kolumn.
     * @param {object} conditions Obiekt zawierający warunki zapytania.
     * @param {string} _columns Opcjonalny parametr - kolumny do wybrania, domyślnie wszystkie (*).
     * @param {string} _tabela Opcjonalny parametr - nazwa tabeli, domyślnie wartość tableName z obiektu wywołującego.
     * @returns {Promise<Array<Object>>} Promise zawierający wyniki zapytania w postaci tablicy obiektów.
     */
    async selectParamRef(RecObj, conditions, _columns, _tabela, _joinTable) {
        let whereClause = '';
        let values = [];

        // Tworzenie warunków WHERE na podstawie przekazanych warunków
        if (conditions && typeof conditions === 'object' && Object.keys(conditions).length > 0) {
            const whereConditions = Object.keys(conditions).map((key, index) => {
                values.push(conditions[key]);
                return `${key} = $${index + 1}`;
            });
            whereClause = ` WHERE ${whereConditions.join(' AND ')}`;
        }

        let cols = ' * ';
        let tabela = '';

        // Sprawdzenie, czy został przekazany opcjonalny parametr _columns
        if (_columns !== undefined) {
            cols = _columns;
        }

        // Sprawdzenie, czy został przekazany opcjonalny parametr _tabela
        if (_tabela !== undefined) {
            tabela = _tabela;
        } else {
            tabela = this.tableName;
        }
        let sql ='';
        if(_joinTable!== undefined && _joinTable.table != undefined && _joinTable.table!= '')
        {
            sql = `SELECT ${cols},${_joinTable.columns} FROM ${tabela} JOIN ${_joinTable.table} ${_joinTable.clausule}  ${whereClause};`;
        }
        else
         sql = `SELECT ${cols} FROM ${tabela}${whereClause};`;

        try {
            // Wykonanie zapytania do bazy danych
            const result = await pool.query(sql, values);
            // Zaktualizowanie zawartości obiektu RecObj
           // RecObj.data = result.rows;
            Object.assign(RecObj, result.rows[0]);
            // Zwrócenie wartości true, jeśli zapytanie zostało wykonane pomyślnie
            return true;
        } catch (error) {
            console.log(error);
            // Zwrócenie wartości false w przypadku błędu
            return false;
        }
    }

    async insert(mObject) {
        let bStat = true;
        const columns = Object.keys(mObject).slice(2).filter(key => key !== 'tableName').join(', ');
        console.log('kolumny :');
        console.log((columns));
        const values = Object.values(mObject).slice(2).filter(value => value !== mObject.tableName);
        const placeholders = values.map((_, i) => `$${i + 1}`).join(', ');
        const sql = `INSERT INTO ${mObject.tableName} (${columns}) VALUES (${placeholders}) RETURNING *;`;
        logger.debug(`insert: ${sql} ${values}`)
       // const sukces = await wykonajZapytanie(client.query('INSERT INTO tabela (kolumna) VALUES ($1)', ['wartość']));

         bStat = bStat &&  await this.wykonajZapytanie( pool.query(sql, values) , mObject );
      //  logger.debug(`inserted: ${JSON.stringify(result.rows[0])}`)
       // Object.assign(mObject, result.rows[0]);

        return bStat;
    }

    async updateRec(rec) {
        // const entries = Object.entries(rec).slice(2).filter(([key]) => key !== 'tableName');
        const entries = Object.entries(rec).slice(2).filter(([key, value]) => key !== 'tableName' && value !== null);

        //   const setClauses = entries.map(([col], i) => `${col} = $${i + 1}`).join(', ');
        // const values = entries.map(([_, val]) => val);
        const setClauses = entries.map(([col, val]) => `${col} = '${val}'`).join(', ');
        const sql = `UPDATE ${rec.tableName} SET ${setClauses} WHERE trd_id = $1 RETURNING *;`;
        console.log(setClauses); // do debugowania
        // np. await db.query(sql, values);
        var result = await pool.query(sql, [1]);
    }

    async update(Rec, _id = Rec[Object.keys(Rec)[1]], defIdKol = Object.keys(Rec)[1]) {
        const id_kol = defIdKol;
        const entries = Object.entries(Rec).slice(2).filter(([key]) => key !== 'tableName');
        const setClauses = entries.map(([col], i) => `${col} = $${i + 1}`).join(', ');
        const values = entries.map(([_, val]) => val);
        values.push(_id);
        const sql = `UPDATE ${this.tableName} SET ${setClauses} WHERE ${id_kol} = $${values.length} RETURNING *;`;
        logger.debug(`UPDATE: ${sql} ${values}`)
        var result = await pool.query(sql, values);
        logger.debug(`updated: ${JSON.stringify(result.rows[0])}`)
        return result.rows[0];
    }

    async updateRecObj(rec, tableName, kolumn_id, val_id) {
        // const entries = Object.entries(rec).slice(2).filter(([key]) => key !== 'tableName');
        const entries = Object.entries(rec).slice(1).filter(([key, value]) => key !== 'tableName' && value !== null);
        console.log(entries);
        //   const setClauses = entries.map(([col], i) => `${col} = $${i + 1}`).join(', ');
        // const values = entries.map(([_, val]) => val);
        const setClauses = entries.map(([col, val]) => `${col} = '${val}'`).join(', ');
        const sql = `UPDATE ${tableName} SET ${setClauses} WHERE ${kolumn_id} = $1 RETURNING *;`;
        console.log(sql); // do debugowania
        // np. await db.query(sql, values);
        let result = {};
        try {
            result = await pool.query(sql, [val_id]);
            return result.rows;
        } catch (error) {
            console.log(error);
        }


    }


}

module.exports = BaseDB;
