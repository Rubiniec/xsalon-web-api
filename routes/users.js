var express = require('express');
var router = express.Router();
const controller = require('../controllers/users.controller');
/* GET users listing. */
router.post('/get',controller.getUser);
router.post('/add',controller.addUser);
module.exports = router;
