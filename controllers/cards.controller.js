

const cardService = require('../services/cards.service');


async function getCard(req, res) {
    try {
        const towar = await cardService.getCard(req.body);
        res.status(200).json(towar);
    } catch (error) {
        console.log(error);
        res.send("Error: " + error);
    }
}

module.exports = {
    getCard
}
